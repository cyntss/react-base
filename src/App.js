import React from 'react'
import { Router } from '@reach/router'
import './assets/scss/index.scss'
import Home from './pages/Home'

function App() {
  return (
    <div className='App'>
      <Router primary={false}>
        <Home path='/' />
      </Router>
    </div>
  )
}

export default App
